<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\User\Dto\Registration;
use App\User\UserService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class User extends Fixture
{
    protected $roles = [
        'ROLE_USER'
    ];

    /**
     * @var UserService
     */
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }


    public function load(ObjectManager $manager): void
    {
        foreach ($this->roles as $role) {
            $userName = explode('_', $role)[1];
            $user = new Registration();
            $user->setUsername($userName);
            $user->setPassword($userName . $userName);
            $user->setEmail($userName . '@email.ru');
            $this->userService->register($user);
        }
        $manager->flush();
    }
}