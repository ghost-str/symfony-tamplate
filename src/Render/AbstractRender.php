<?php


namespace App\Render;


use Symfony\Component\HttpFoundation\Response;

abstract class AbstractRender implements RenderInterface
{
    protected const CONTENT_TYPE_HEADER = 'Content-Type';
    protected const CONTENT_TYPE_HTML = 'text/html';

    protected function createResponse(string $content, array $responseParams): Response
    {
        return new Response($content, $responseParams['status'] ?? 200, $responseParams['headers'] ?? []);
    }
}