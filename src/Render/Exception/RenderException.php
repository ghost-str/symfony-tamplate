<?php


namespace App\Render\Exception;


use Throwable;

class RenderException extends \Exception
{
    public function __construct($message = '', $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}