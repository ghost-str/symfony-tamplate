<?php


namespace App\Render;


use Symfony\Component\HttpFoundation\Response;

interface RenderInterface
{
    public function render($data, array $params = [], array $responseParams = []): Response;
}