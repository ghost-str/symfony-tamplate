<?php


namespace App\Render;


use App\Render\Exception\RenderException;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class TwigRender extends AbstractRender
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * TwigRender constructor.
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @param $data
     * @param array $params
     * @param array $responseParams
     * @return Response
     * @throws RenderException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render($data, array $params = [], array $responseParams = []): Response
    {
        if (!isset($params['template'])) {
            throw new RenderException('Param template mast be set');
        }
        $content = $this->twig->render($params['template'], $data??[]);

        $responseParams = array_merge_recursive([
            'headers' => [
                self::CONTENT_TYPE_HEADER => self::CONTENT_TYPE_HTML
            ]
        ], $responseParams);

        return $this->createResponse($content, $responseParams);
    }
}