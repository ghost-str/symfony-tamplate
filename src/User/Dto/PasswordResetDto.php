<?php


namespace App\User\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class PasswordResetDto
{
    /**
     * @Assert\NotBlank
     * @Assert\Length(min=6,max=255)
     * @var
     */
    private $emailOrUsername;

    /**
     * @return mixed
     */
    public function getEmailOrUsername()
    {
        return $this->emailOrUsername;
    }

    /**
     * @param mixed $emailOrUsername
     * @return PasswordResetDto
     */
    public function setEmailOrUsername($emailOrUsername): PasswordResetDto
    {
        $this->emailOrUsername = $emailOrUsername;
        return $this;
    }


}