<?php
declare(strict_types=1);

namespace App\User\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class Registration
{
    /**
     * @Assert\NotBlank
     * @Assert\Length(min=5,max=255)
     * @var string
     */
    private $username;
    /**
     * @Assert\NotBlank
     * @Assert\Email
     * @var string
     */
    private $email;
    /**
     * @Assert\NotBlank
     * @Assert\Length(min=6,max=4096)
     * @var string
     */
    private $password;

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Registration
     */
    public function setUsername(string $username): Registration
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Registration
     */
    public function setEmail(string $email): Registration
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Registration
     */
    public function setPassword(string $password): Registration
    {
        $this->password = $password;
        return $this;
    }

}