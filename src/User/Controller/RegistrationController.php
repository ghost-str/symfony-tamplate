<?php
declare(strict_types=1);

namespace App\User\Controller;

use App\User\Form\RegistrationFormType;
use App\Render\RenderInterface;
use App\User\Dto\Registration;
use App\User\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;


class RegistrationController
{
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var FormInterface
     */
    private $form;

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var RenderInterface
     */
    private $render;
    /**
     * @var TokenInterface
     */
    private $token;
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;
    /**
     * @var Security
     */
    private $security;

    public function __construct(
        UserService $userService,
        FormFactoryInterface $form,
        EntityManagerInterface $em,
        RenderInterface $render,
        Security $security,
        UrlGeneratorInterface $urlGenerator
    )
    {
        $this->userService = $userService;
        $this->form = $form;
        $this->em = $em;
        $this->render = $render;
        $this->security = $security;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @Route("/register", name="registration")
     * @param Request $request
     * @return Response
     */
    public function register(Request $request): Response
    {
        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return new RedirectResponse($this->urlGenerator->generate('home'));
        }

        $registration = new Registration();
        $form = $this->form->create(RegistrationFormType::class, $registration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->userService->register($registration);
            $this->em->flush();
            return new RedirectResponse($this->urlGenerator->generate('home'));
        }
        return $this->render->render([
            'registrationForm' => $form->createView(),
        ], [
            'template' => 'user/register.html.twig'
        ]);
    }
}
