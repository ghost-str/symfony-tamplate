<?php
declare(strict_types=1);

namespace App\User\Controller;


use App\User\Form\ResetPasswordType;
use App\Render\RenderInterface;
use App\User\UserService;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RestorePasswordController
{
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var FormFactoryInterface
     */
    private $form;
    /**
     * @var RenderInterface
     */
    private $render;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(
        UserService $userService,
        FormFactoryInterface $form,
        RenderInterface $render,
        UrlGeneratorInterface $urlGenerator
    )
    {
        $this->userService = $userService;
        $this->form = $form;
        $this->render = $render;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @Route("reset",name="reset_password_string")
     * @param Request $request
     * @return Response
     */
    public function ResetString(Request $request): Response
    {
        $form = $this->form->create(ResetPasswordType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->userService->restorePassword($form)) {
                return new RedirectResponse($this->urlGenerator->generate('home'));
            }
        }
        return $this->render->render(['form' => $form->createView()], ['template' => 'user/resetPassword.html.twig']);
    }
}