<?php
declare(strict_types=1);

namespace App\User;


use App\User\Entity\PasswordResetToken;
use App\User\Entity\User;
use App\User\Dto\Registration;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;
    /**
     * @var UserPasswordEncoderInterface
     */
    protected $passwordEncoder;

    /**
     * @var int
     */
    protected $tokenDuration;

    public function __construct(
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder,
        int $tokenDuration = 3600
    )
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
        $this->tokenDuration = $tokenDuration;
    }

    public function register(Registration $registrationDto): void
    {
        $user = new User();
        $user->setUsername($registrationDto->getUsername());
        $user->setEmail($registrationDto->getEmail());
        $password = $registrationDto->getPassword();
        $password = $this->passwordEncoder->encodePassword($user, $password);
        $user->setPassword($password);
        $this->em->persist($user);
    }

    public function restorePassword(FormInterface $form): bool
    {
        if (!$form->isValid()) {
            return false;
        }

        $email = $form->get('email')->getData();

        $repository = $this->em->getRepository(User::class);

        $user = $repository->findOneBy(['email' => $email]);

        if (null === $user) {
            return false;
        }

        $passwordResetToken = new PasswordResetToken();
        $passwordResetToken->setToken(random_bytes(255));
        $dataTime = new \DateTime('+ ' . $this->tokenDuration . ' seconds');
        $passwordResetToken->setExpired($dataTime);
        $user->setPasswordResetToken($passwordResetToken);

        $this->em->persist($user);
        return true;
    }
}