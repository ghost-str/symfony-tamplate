<?php


namespace App\Controller;


use App\Render\RenderInterface;
use Symfony\Component\Routing\Annotation\Route;

class Controller
{
    /**
     * @var RenderInterface
     */
    private $render;
    public function __construct(RenderInterface $render)
    {
        $this->render=$render;
    }

    /**
     * @Route("",name="home")
     */
    public function main(){
        return $this->render->render(null,['template'=>'home.html.twig']);
    }
}