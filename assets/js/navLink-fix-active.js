(function () {
    const location = window.location.pathname;
    const navLinks = document.getElementsByClassName('nav-link');
    const span = document.createElement('span');
    span.classList.add('sr-only');
    span.innerText = '(current)';
    for (let navLink of navLinks) {
        const attr = navLink.getAttribute('href');
        if (attr != null && location === attr) {
            navLink.classList.add('active');
            navLink.appendChild(span);
        }
    }
})();