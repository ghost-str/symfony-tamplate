<?php

return [
    'Username' => 'Имя пользователя',
    'Email' => 'Email',
    'Password' => 'Пароль',
    'Agree terms' => 'Согласиться',
    'Register action' => 'Зарегестрироваться',
    'Register'=>'Регистрация',
    'Home'=>'Главная'
];